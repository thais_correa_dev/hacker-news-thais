require 'rails_helper'

RSpec.describe SearchsController, type: :controller do


  describe "GET #index" do
    before(:each) do
      get :index, params:{
        search: "Internet"
      }
    end

    it "should have http response 200" do
      expect(response).to have_http_status(200)
    end

    it "should have a stories variable with less or 10 elements" do
      expect(assigns(:stories).count).to be <= 10
    end

    it "should have a word variable equals to params[:search]" do
      expect(assigns(:word)).to eq "Internet"
    end

  end

end
