require 'rails_helper'

RSpec.describe NewsController, type: :controller do


  describe "GET #home" do
    before(:each) do
      get :home
    end

    it "should have http response 200" do
      expect(response).to have_http_status(200)
    end

    it "should have a stories variable with 15 elements" do
      expect(assigns(:stories).count).to eq 15
    end

  end

end
