require 'rails_helper'

RSpec.describe StoriesController, type: :controller do


  describe "GET #show" do
    before(:each) do
      get :show, params:{
        id: "8863"
      }
    end

    it "should have http response 200" do
      expect(response).to have_http_status(200)
    end

    it "should have a stories variable with 15 elements" do
      expect(assigns(:story)).to_not be nil
    end

  end

end
