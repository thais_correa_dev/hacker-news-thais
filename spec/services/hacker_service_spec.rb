require 'rails_helper'
describe HackerService do
  let(:HackerService) { described_class.new }

  describe "when the method get_top_15 is called" do
    it 'should return a array with 15 elements' do
      res = HackerService.new.get_top_15
      expect(res.count).to eq 15
    end

    it 'each element should have a story' do
      res = HackerService.new.get_top_15
      expect(res.first[:story]).to_not be nil
    end

    it 'each element should have comments' do
      res = HackerService.new.get_top_15
      expect(res.first[:comments]).to_not be nil
    end
  end

  describe "when the method get_comments is called" do
    before(:each) do 
      @id = "8863"
    end

    it 'should return a json' do
      res = HackerService.new.get_comments @id
      expect(res).to_not be nil
    end

    it 'the response should have a story' do
      res = HackerService.new.get_comments @id
      expect(res[:story]).to_not be nil
    end

    it 'the response should have comments' do
      res = HackerService.new.get_comments @id
      expect(res[:comments]).to_not be nil
    end

    it 'the text of a comment should have more than 20 words ' do
      res = HackerService.new.get_comments @id
      expect(res[:comments].first['text'].scan(/\w+/).size).to be > 20
    end
  end

  describe "when the method search is called" do

    it 'should return a array with 10 elements' do
      res = HackerService.new.search "Internet"
      expect(res.count).to be <= 10
    end

    it 'each element should have a time' do
      res = HackerService.new.search "Internet"
      expect(res.first[:time]).to_not be nil
    end

    it 'each element should have a title' do
      res = HackerService.new.search "Internet"
      expect(res.first[:title]).to_not be nil
    end

    it 'each element should have a url' do
      res = HackerService.new.search "Internet"
      expect(res.first[:url]).to_not be nil
    end

    it 'each element should have comments' do
      res = HackerService.new.search "Internet"
      expect(res.first[:comments]).to_not be nil
    end

    it 'each element should have a id' do
      res = HackerService.new.search "Internet"
      expect(res.first[:id]).to_not be nil
    end
  end


end
