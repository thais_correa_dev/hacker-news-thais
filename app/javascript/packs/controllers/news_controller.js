import ActionCable from 'actioncable';
import { Controller } from "stimulus"

export default class extends Controller {
	static targets = ["news"]

  connect() {

	  var cable = ActionCable.createConsumer();
	  cable.subscriptions.create('NewsChannel', {
			received: (data) => {
			  console.log('received', data);
			  var element = $("#news")
			  $.ajax ({
	        url: "/",
	        method: 'GET',
	        dataType: 'json',
	        data:{
	          channel: "true"
	        },
	        success: function(data) {
					  var new_stories = $(data.partial)
		        element.html(new_stories)
		      }
				})
			}
	  });
	}
}