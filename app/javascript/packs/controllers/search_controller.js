import { Controller } from "stimulus"

export default class extends Controller {
  connect() {
    var word = document.getElementsByClassName("word")
		var word = word[0].dataset["target"]
    var input = document.getElementsByClassName("text")
    for (var i=0, max=input.length; i < max; i++) {
			var output = input[i].outerHTML.replace(new RegExp('(' + word + ')',"i"), '<span id="word">$1</span>');
			input[i].outerHTML= output
		}
  }
}