require "i18n"

class SearchsController < ApplicationController 
	def index
		@stories = HackerService.new.search I18n.transliterate(params[:search])
		@word = I18n.transliterate(params[:search])
	end
end
