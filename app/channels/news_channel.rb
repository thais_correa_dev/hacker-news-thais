class NewsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "news"
  end

  def unsubscribed
    stop_all_streams
  end
end