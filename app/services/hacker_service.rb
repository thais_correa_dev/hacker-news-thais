require 'nokogiri'
require 'open-uri'
require 'thread'
require 'thwait'

class HackerService
	include HTTParty

	def initialize
		@base_url = "https://hacker-news.firebaseio.com/v0/"
	end


	def get_top_15
		story = []
		stories = []
		 
		response = HTTParty.get(@base_url + "topstories.json?print=pretty")
    ids = response.first(15)
 
    ids.each do |id|
 
      story << Thread.new{
 
        res = HTTParty.get(@base_url+'item/'+id.to_s+'.json?print=pretty')
				if res['kids']
					comments = res['kids'].count
				else
					comments = 0
				end
				stories.push({story: res, comments: comments})
 
      }
 
    end
 
    ThreadsWait.all_waits(*story)
		 
		stories.sort {|a, b| b[:story]['time'] <=> a[:story]['time']}

	end

	def get_comments story_id
		comment = []
		comments = []
		 
		response = HTTParty.get(@base_url+'item/'+story_id.to_s+'.json?print=pretty')
 
    ids = response['kids']
 
    ids.each do |id|
 
      comment << Thread.new{
 
        res = HTTParty.get(@base_url+'item/'+id.to_s+'.json?print=pretty')
				if res['text']
					if res['text'].scan(/\w+/).size > 20
						res['text'] = res['text'].html_safe
						comments.push(res)
					end
				end 
      }
 
    end
 
    ThreadsWait.all_waits(*comment)
		 
		{story: response, comments: comments.sort {|a, b| b['time'] <=> a['time']}}
	end

	def search string
		array = []
		response = HTTParty.get('http://hn.algolia.com/api/v1/search?query='+string+'&tags=story')
		search = response.first.last.first(10)
		search.each do |story|
			array.push({
				time: story['created_at'],
				title: story['title'],
				url: story['url'],
				comments: story['num_comments'],
				id: story["objectID"]
			})
		end
		array.sort {|a, b| b[:time] <=> a[:time]}
	end
end